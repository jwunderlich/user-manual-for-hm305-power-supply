# User Manual for Hanmatek HM305 Power Supply

Cleanly created operating instructions for the HM305 power supply, based on the printed original in English and German.
Unfortunately, the printed documentation is poor quality, so I created a digital version with LibreOffice.

The instructions are without any claim to completeness. All trademark rights are with the manufacturer.

| File descriptions:        |                                                        |
|---------------------------|--------------------------------------------------------|
| [HANMATEK_HM305_PSU_en.pdf](HANMATEK_HM305_PSU_en.pdf) | HM305-User Manual english in PDF Format                |
| HANMATEK_HM305_PSU_en.odt | HM305-User Manual english in Libreoffice Writer Format |
| HM305_front.odg           | HM305-Drawing Front Libreoffice Draw Format            |
| HM305_rear.odg            | HM305-Drawing Rear Libreoffice Draw Format             |
| HM305_dimensions.odg      | HM305-Drawing perspectival Libreoffice Draw Format     |

Software requirements for processing the documents:
- Libreoffice (Writer, Draw)
- [DSEG](https://www.keshikan.net/fonts-e.html) 7-segment and 14-segment fonts

# Bedienungsanleitung für Hanmatek HM305 Netzteil

Sauber erstellte Bedienungsanleitung für das HM305 Netzteil, basierend auf dem gedruckten Original in englisch und deutsch.
Leider ist die gedruckte Dokumentation minderwertig, so habe ich eine digitale Version mit LibreOffice erstellt.

Die Anleitung ist ohne Anspruch auf Vollständigkeit. Alle Markenrechte sind beim Hersteller.

| Beschreibung der Dateien: |                                                        |
|---------------------------|--------------------------------------------------------|
| [HANMATEK_HM305_PSU_de.pdf](HANMATEK_HM305_PSU_de.pdf) | HM305-Bedienungsanleitung Deutsch in PDF Format        |
| HANMATEK_HM305_PSU_de.odt | HM305-Bedienungsanleitung Deutsch in Libreoffice Writer Format|
| HM305_front.odg           | HM305-Zeichnung Front Libreoffice Draw Format          |
| HM305_rear.odg            | HM305-Zeichnung Rückseite Libreoffice Draw Format      |
| HM305_dimensions.odg      | HM305-Zeichnung Perspektivisch Libreoffice Draw Format |[]([url](url))

Softwareanforderungen für die Bearbeitung der Dokumente:
- Libreoffice (Writer, Draw)
- [DSEG](https://www.keshikan.net/fonts-e.html) 7-segment und 14-segment Schriftart
